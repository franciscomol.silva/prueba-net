﻿using DecoderJWT.Service;
using EncoderJWT.Service;
using ParametrosJWT.Configuration;
using ParametrosJWT.Entity;
using System;
using System.Collections.Generic;

namespace ConsolaUnitaria
{
    class Program
    {
        static void Main(string[] args)
        {
            string seed = "Dimension12345";
            bool sw = true;
            string opcion = "-1";
            Console.WriteLine("Indique que desea hacer");
            Console.WriteLine("1: Crear Token");
            Console.WriteLine("2: Descomponer Token");

            while (sw)
            {
                opcion = Console.ReadLine();

                if (opcion.Equals("1") || opcion.Equals("2"))
                {
                    sw = false;
                }
                else
                {
                    Console.WriteLine("Por favor ingrese un valor valido");
                    Console.WriteLine("---------------------------------");
                    Console.WriteLine("Indique que desea hacer");
                    Console.WriteLine("1: Crear Token");
                    Console.WriteLine("2: Descomponer Token");

                }
            }

            switch (opcion)
            {
                case "1":
                    ICreaJWT creaJwt = new CreaJWT(seed, new ConfigurationJWT());
                    Console.WriteLine(creaJwt.GeneraJWT(new Account() { Id = "123", Token = "Token pulentoso", Usuario = "usuario@dim.cl", Perfiles = new string[] { "peril 1", "perfil 2", "admin" } }));

                    break;
                case "2":
                    ILeeJWT leeJWT = new LeeJWT(seed, new ConfigurationJWT());

                    Console.WriteLine("Por favor ingrese token");
                    string token = Console.ReadLine();

                    Console.WriteLine("---");
                    Console.WriteLine(leeJWT.DecorerJWT(token));
                    Console.WriteLine("---");
                    break;
            }

            Console.ReadLine();
        }
    }
}
