﻿using JWT;
using JWT.Algorithms;
using JWT.Serializers;

namespace ParametrosJWT.Configuration
{
    public class ConfigurationJWT : IConfigurationJWT
    {
        private int _timeExpired;

        public ConfigurationJWT(int timeExpired)
        {
            _timeExpired = timeExpired;
        }

        public ConfigurationJWT()
        {
            _timeExpired = 3600;
        }

        public HMACSHA256Algorithm Algorithm
        {
            get
            {
                return new HMACSHA256Algorithm();
            }
        }
        public JsonNetSerializer Serializer
        {
            get
            {
                return new JsonNetSerializer();
            }
        }
        public JwtBase64UrlEncoder UrlEncoder
        {
            get
            {
                return new JwtBase64UrlEncoder();
            }
        }

        public UtcDateTimeProvider Provider
        {
            get
            {
                return new UtcDateTimeProvider();
            }
        }

        public int TimeExpired
        {
            get
            {
                return _timeExpired;
            }
        }
    }
}
