﻿using JWT;
using JWT.Algorithms;
using JWT.Serializers;

namespace ParametrosJWT.Configuration
{
    public interface IConfigurationJWT 
    {
        HMACSHA256Algorithm Algorithm { get; }
        JsonNetSerializer Serializer { get; }
        JwtBase64UrlEncoder UrlEncoder { get; }
        UtcDateTimeProvider Provider { get; }
        int TimeExpired { get; }


    }
}
