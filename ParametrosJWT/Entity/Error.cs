﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParametrosJWT.Entity
{
    public class Error
    {
        private int _codigo = 0;

        public int Codigo {
            get
            {
                return _codigo;
            }

            set
            {
                _codigo = value;
            }
            
        }
        public string Mensaje { get; set; }
    }
}
