using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParametrosJWT.Entity
{
    public class Account
    {
        public double exp { get; set; }
        public string Usuario { get; set; }
        public string Token { get; set; }
        public string Id { get; set; }
        public string[] Perfiles { get; set; }
    }
}
