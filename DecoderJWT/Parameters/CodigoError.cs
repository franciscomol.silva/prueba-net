﻿namespace DecoderJWT.Parameters
{
    public class CodigoError
    {
        /// <summary>
        /// Códigos de retorno para caso de errores
        /// </summary>
        ///     -1: Error no controlado
        ///      1: Token no valido (No respeta la base del encoder)
        ///      2: Firma no valida
        ///      3: Token expirado
        ///      4: Token vacio
        ///      5: Seed vacio
        public enum CodigoRetorno
        {
            ErrorNoControlado = -1,
            TokenNoValido = 1,
            FirmaNoValida = 2,
            TokenExpirado = 3,
            TokenVacio = 4,
            SeedVacio = 5
        }
    }
}
