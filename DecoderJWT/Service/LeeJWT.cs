﻿using DecoderJWT.Helper;
using JWT;
using JWT.Exceptions;
using ParametrosJWT.Configuration;
using ParametrosJWT.Entity;
using System;

namespace DecoderJWT.Service
{
    public class LeeJWT : ILeeJWT
    {
        readonly IJwtValidator _validator;
        IConfigurationJWT _configuration;
        IJwtDecoder _decoder;
        private string _seed;

        public LeeJWT(string seed, IConfigurationJWT configuration)
        {
            _configuration = configuration;
            _seed = seed;
            _validator = new JwtValidator(_configuration.Serializer, _configuration.Provider, _configuration.TimeExpired);
            _decoder = new JwtDecoder(_configuration.Serializer, _validator, _configuration.UrlEncoder, _configuration.Algorithm);
        }

        /// <summary>
        /// Método que decodifica JWT
        /// </summary>
        /// <param name="token">Token firmado</param>
        /// <param name="seed">Semilla utilizada pra firmar Token</param>
        /// <returns>
        /// En caso de éxito retorna json con la información que se encuentre en el Token
        /// Si sucede un error proporcionará un JSon con el código de error y una breve descripción
        /// ---
        /// Códigos de error
        ///     -1: Error no controlado
        ///      1: Token no valido (No respeta la base del encoder)
        ///      2: Firma no valida
        ///      3: Token expirado
        ///      4: Token vacio
        ///      5: Seed vacio
        /// </returns>
        public string DecorerJWT(string token)
        {
            string json = "";
            try
            {
                if (!String.IsNullOrEmpty(token))
                {
                    if (!String.IsNullOrEmpty(_seed))
                    {
                        json = _decoder.Decode(token, _seed, verify: true);
                    }
                    else
                    {
                        json = ResponseJSon.ResponseError(new Error() { Codigo = 5, Mensaje = "Seed vacia" });
                    }
                }
                else
                {
                    json = ResponseJSon.ResponseError(new Error() { Codigo = 4, Mensaje = "Token vacio" });
                }

            }
            catch (TokenExpiredException)
            {

                json = ResponseJSon.ResponseError(new Error() { Codigo = 3, Mensaje = "Token Expirado" });
            }
            catch (SignatureVerificationException)
            {
                json = ResponseJSon.ResponseError(new Error() { Codigo = 2, Mensaje = "Firma no valida" });

            }
            catch (FormatException)
            {
                json = ResponseJSon.ResponseError(new Error() { Codigo = 1, Mensaje = "Token no valido (formato no corresponde a la base de encoding)" });
            }
            catch (Exception ex)
            {
                json = ResponseJSon.ResponseError(new Error() { Codigo = -1, Mensaje = ex.Message });
            }

            return json;
        }
    }
}
