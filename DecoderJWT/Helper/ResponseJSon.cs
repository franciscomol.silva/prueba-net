﻿using ParametrosJWT.Entity;
using Newtonsoft.Json;


namespace DecoderJWT.Helper
{
    public class ResponseJSon
    {

        public static string ResponseError(Error error)
        {
            string json = "";

            json = JsonConvert.SerializeObject(error);

            return json;
        }
    }
}
