﻿using DecoderJWT.Service;
using FluentAssertions;
using ParametrosJWT.Configuration;
using UnitTestDecoderJWT.Data;
using Xunit;

namespace UnitTestDecoderJWT
{
    public class UnitTestLeeJWT
    {
        private ILeeJWT _leeJwt;

        public UnitTestLeeJWT()
        {
            var seed = "secret";
            _leeJwt = new LeeJWT(seed, new ConfigurationJWT());
        }

        [Theory]
        [ClassData(typeof(DecoderData))]
        public void Should_Be_Decode(string token, string seed, string json)
        {
            //Arrange

            //Act
            var result = _leeJwt.DecorerJWT(token);

            //Assert
            result.Should().Be(json);
        }

        [Fact]
        public void Should_Be_Error_Seed_Empty()
        {
            //Arrange
            string token = "123";
            string seed = "";
            string json = "{\"Codigo\":5,\"Mensaje\":\"Seed vacia\"}";
            
            //Act
            var result = _leeJwt.DecorerJWT(token);

            //Assert
            result.Should().Be(json);
        }

        [Fact]
        public void Should_Be_Error_Token_Empty()
        {
            //Arrange
            string token = "";
            string seed = "123";
            string json = "{\"Codigo\":4,\"Mensaje\":\"Token vacio\"}";

            //Act
            var result = _leeJwt.DecorerJWT(token);

            //Assert
            result.Should().Be(json);
        }

        [Fact]
        public void Should_Be_Error_Expired_Token()
        {
            //Arrange
            string token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOiIxNTc3ODQ3NjAwIiwiVXN1YXJpbyI6ImNvcnJlb18zQGRpbS5jbCIsIlBlcmZpbCI6IjIzIiwiVG9rZW4iOiIxMjMyZjR0eWg2LUg0MjMydHI0NTQtZzQ0LWc0NGdkMzZ0di0yMiJ9.Ky7xo1s4slsPpDBlm9mSPUGUAnCKFpbFJGxwI95UsQk";
            string seed = "secret";
            string json = "{\"Codigo\":3,\"Mensaje\":\"Token Expirado\"}";

            //Act
            var result = _leeJwt.DecorerJWT(token);

            //Assert
            result.Should().Be(json);
        }

        [Fact]
        public void Should_Be_Error_Signatured_not_valid()
        {
            //Arrange
            string token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJVc3VhcmlvIjoiY29ycmVvQGNvcnJlby5jbCIsIlBlcmZpbCI6IjEiLCJUb2tlbiI6ImFzZGZnaGprbMOxIn0.KrnONE_TPvf1oPZ4ohJFsiXhP9GLCUB-CVjdp3NoI7c";
            string seed = "123";
            string json = "{\"Codigo\":2,\"Mensaje\":\"Firma no valida\"}";

            //Act
            var result = _leeJwt.DecorerJWT(token);

            //Assert
            result.Should().Be(json);
        }

        /// <summary>
        /// Para que este error suceda debe estar incoherente la sección PayLoad (DATA) o Header o la firma        
        /// Header Malo
        /// </summary>
        [Fact]
        public void Should_Be_Error_Token_not_valid_whit_header()
        {
            //Arrange
            string token = "eyJhbGciOiJIUzI1NiIsInR5cCI6dIkpXVCJ9.eyJkYXRvIjoiZGF0b29vbyJ9.eBgPUXp1iF1yGgComF0CCgB-Xnl9Vi-KOBP-BcuoYDE";
            string seed = "secret";
            string json = "{\"Codigo\":1,\"Mensaje\":\"Token no valido (formato no corresponde a la base de encoding)\"}";

            //Act
            var result = _leeJwt.DecorerJWT(token);

            //Assert
            result.Should().Be(json);
        }

        /// <summary>
        /// Para que este error suceda debe estar incoherente la sección PayLoad (DATA) o Header o la firma        
        /// </summary>
        [Fact]
        public void Should_Be_Error_Token_not_valid_whit_PayLoad()
        {
            //Arrange            
            string token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOiIxNTc3ODQ3NjAwIiwiVXN1YXJpbyI6ImNvcnJlb18zQGRpbS5jbCIsIlBlcmZpbCI6IjIzIiwiVG9rZW4iOiIxMjMyZjR0eWgd2LUg0MjMydHI0NTQtZzQ0LWc0NGdkMzZ0di0yMiJ9.Ky7xo1s4slsPpDBlm9mSPUGUAnCKFpbFJGxwI95UsQk";
            string seed = "secret";
            string json = "{\"Codigo\":1,\"Mensaje\":\"Token no valido (formato no corresponde a la base de encoding)\"}";

            //Act
            var result = _leeJwt.DecorerJWT(token);

            //Assert
            result.Should().Be(json);
        }

        /// <summary>
        /// Para que este error suceda debe estar incoherente la sección PayLoad (DATA) o Header o la firma        
        /// </summary>
        [Fact]
        public void Should_Be_Error_Token_not_valid_whit_SIGNATURE()
        {
            //Arrange
            string token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflsKxwRJSMeKKF2QT4fwpMeJf36dPOk6yJV_adQssw5c";
            string seed = "secret";
            string json = "{\"Codigo\":1,\"Mensaje\":\"Token no valido (formato no corresponde a la base de encoding)\"}";

            //Act
            var result = _leeJwt.DecorerJWT(token);

            //Assert
            result.Should().Be(json);
        }

        [Fact]
        public void Should_Be_Error_Not_Controlled()
        {
            //Arrange
            string token = "12345tg";
            string seed = "123";
            string json = "{\"Codigo\":-1,\"Mensaje\":\"Token must consist of 3 delimited by dot parts.\\r\\nParameter name: token\"}";

            //Act
            var result = _leeJwt.DecorerJWT(token);

            //Assert
            result.Should().Be(json);
        }
    }
}
