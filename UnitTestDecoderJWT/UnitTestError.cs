﻿using ParametrosJWT.Entity;
using FluentAssertions;
using Xunit;

namespace UnitTestDecoderJWT
{
    public class UnitTestError
    {
        [Theory]
        [InlineData(123, "Mensaje enviado")]
        public void Should_Be_Return_Value(int _codigo, string _mensaje)
        {
            //Arrange
            Error error = new Error() { Codigo = _codigo, Mensaje = _mensaje };
            //Act
            int codigo = error.Codigo;
            string mensaje = error.Mensaje;

            //Assert
            codigo.Should().Be(_codigo);
            mensaje.Should().Be(_mensaje);
        }
    }
}
