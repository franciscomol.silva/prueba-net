﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace UnitTestDecoderJWT.Data
{
    public class DecoderData: TheoryData<string, string, string>
    {
        public DecoderData()
        {
            Add("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc3VhcmlvIjoiY29ycmVvMUBkaW0uY2wiLCJQZXJmaWwiOiIxIiwiVG9rZW4iOiIxMjMyZjR0eWg2LUg0dHI0NTQtZzQ0LWc0NGdkMzZ0di0yMiJ9.25V8Lye0rRkB6POnVXkIZ2FRvTJxoidZ0RtLx715Oo4", "secret", "{\"Usuario\":\"correo1@dim.cl\",\"Perfil\":\"1\",\"Token\":\"1232f4tyh6-H4tr454-g44-g44gd36tv-22\"}");
            Add("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc3VhcmlvIjoiY29ycmVvMkBkaW0uY2wiLCJQZXJmaWwiOiIxMyIsIlRva2VuIjoiMTIzMmY0dHloNi1INHRyNDU0LWc0NC1nNDRnZDM2dHYtMjIifQ.mN9dlf4ELROx_JyUdAakIq8znt69-UvbCYy_zA_G0dA", "secret", "{\"Usuario\":\"correo2@dim.cl\",\"Perfil\":\"13\",\"Token\":\"1232f4tyh6-H4tr454-g44-g44gd36tv-22\"}");
            Add("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc3VhcmlvIjoiY29ycmVvXzNAZGltLmNsIiwiUGVyZmlsIjoiMjMiLCJUb2tlbiI6IjEyMzJmNHR5aDYtSDQyMzJ0cjQ1NC1nNDQtZzQ0Z2QzNnR2LTIyIn0.LEY6iLjsOw_QlSgYQF4DZrn6Z6Ri5lW89zXEIVW-PcY", "secret", "{\"Usuario\":\"correo_3@dim.cl\",\"Perfil\":\"23\",\"Token\":\"1232f4tyh6-H4232tr454-g44-g44gd36tv-22\"}");
        }
    }
}
