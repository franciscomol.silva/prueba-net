﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace UnitTestDecoderJWT.Data
{
    public class JsonData : TheoryData<int, string, string>
    {
        public JsonData()
        {            
            Add(123, "Mensaje de prueba", "{\"Codigo\":123,\"Mensaje\":\"Mensaje de prueba\"}");
            Add(3, "Mensaje de prueba  2", "{\"Codigo\":3,\"Mensaje\":\"Mensaje de prueba  2\"}");
            Add(156548, "Mensaje de prueba   3", "{\"Codigo\":156548,\"Mensaje\":\"Mensaje de prueba   3\"}");
        }

      
        
    }
}
