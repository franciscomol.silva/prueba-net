﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using FluentAssertions;
using ParametrosJWT.Entity;
using DecoderJWT.Helper;

namespace UnitTestDecoderJWT
{
    public class UnitTestRespondeJson
    {
        [Theory]
        [ClassData(typeof(Data.JsonData))]
        public void Should_Be_Return_Json(int codigo, string mensaje, string json)
        {
            //Arrange            
            Error error = new Error() { Codigo = codigo, Mensaje = mensaje};

            //Act
            var result = ResponseJSon.ResponseError(error);

            //Assert
            result.Should().Be(json);
        }
    }
}
