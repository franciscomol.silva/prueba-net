﻿using FluentAssertions;
using Xunit;
using static DecoderJWT.Parameters.CodigoError;

namespace UnitTestDecoderJWT
{
    public class UnitTestCodigoError
    {

        [Fact]
        public void Should_Be_Return_minus_1()
        {
            //Arrange

            //Act
            var result = CodigoRetorno.ErrorNoControlado;

            //Asseert
            result.Should().Be(-1);
        }

        [Fact]
        public void Should_Be_Return_1()
        {
            //Arrange

            //Act
            var result = CodigoRetorno.TokenNoValido;

            //Asseert
            result.Should().Be(1);
        }

        [Fact]
        public void Should_Be_Return_2()
        {
            //Arrange

            //Act
            var result = CodigoRetorno.FirmaNoValida;

            //Asseert
            result.Should().Be(2);
        }

        [Fact]
        public void Should_Be_Return_3()
        {
            //Arrange

            //Act
            var result = CodigoRetorno.TokenExpirado;

            //Asseert
            result.Should().Be(3);
        }

        [Fact]
        public void Should_Be_Return_4()
        {
            //Arrange

            //Act
            var result = CodigoRetorno.TokenVacio;

            //Asseert
            result.Should().Be(4);
        }

        [Fact]
        public void Should_Be_Return_5()
        {
            //Arrange

            //Act
            var result = CodigoRetorno.SeedVacio;

            //Asseert
            result.Should().Be(5);
        }
    }
}
