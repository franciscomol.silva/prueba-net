﻿using JWT;
using System;

namespace EncoderJWT.Helper
{
    class CalculaTiempoExp
    {
        private IDateTimeProvider _provider;

        public CalculaTiempoExp(IDateTimeProvider provider)
        {
            _provider = provider;
        }

        public double CalculaTiempo()
        {
            DateTimeOffset now = _provider.GetNow();

            double secondsSinceEpoch = UnixEpoch.GetSecondsSince(now); ;

            return secondsSinceEpoch;

        }
    }
}
