﻿using EncoderJWT.Helper;
using JWT;
using ParametrosJWT.Configuration;
using ParametrosJWT.Entity;
using System;


namespace EncoderJWT.Service
{
    public class CreaJWT : ICreaJWT
    {
        private IConfigurationJWT _configuration;
        private IJwtEncoder _encoder;
        private CalculaTiempoExp _calculaTiempoExp;
        private string _seed;
        


        public CreaJWT(string seed, IConfigurationJWT configuration)
        {
            _seed = seed;
            _configuration = configuration;

            _calculaTiempoExp = new CalculaTiempoExp(new UtcDateTimeProvider());
            _encoder = new JwtEncoder(_configuration.Algorithm, _configuration.Serializer, _configuration.UrlEncoder);
        }


        public string GeneraJWT(Account account)
        {
            account.exp = _calculaTiempoExp.CalculaTiempo();
            var token = _encoder.Encode(account, _seed);

            return token;
        }
    }
}
