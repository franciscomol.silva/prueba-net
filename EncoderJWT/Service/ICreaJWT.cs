﻿using ParametrosJWT.Entity;

namespace EncoderJWT.Service
{
    public interface ICreaJWT
    {
        string GeneraJWT(Account account);
    }
}
