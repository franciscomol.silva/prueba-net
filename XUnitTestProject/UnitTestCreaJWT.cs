﻿using EncoderJWT.Service;
using FluentAssertions;
using ParametrosJWT.Configuration;
using ParametrosJWT.Entity;
using Xunit;

namespace UnitTestEncoderJWT
{
    public class UnitTestCreaJWT
    {
        [Theory]
        [ClassData(typeof(Data.TokenData))]
        public void Should_Be_Token(Account cuenta, string assert)
        {
            //Arrange
            CreaJWT jwt = new CreaJWT("123456789", new ConfigurationJWT());

            //Act
            var result = jwt.GeneraJWT(cuenta);

            //Assert
            result.Should().Be(assert);
        }
    }
}
