﻿using EncoderJWT.Helper;
using FluentAssertions;
using System;
using System.Reflection;
using Xunit;

namespace UnitTestEncoderJWT
{
    public class UnitTestVisivility
    {
        [Fact]
        public void Should_Be_Public()
        {
            //Arrange
            string result = "";
            Type type = typeof(Prueba);
            PropertyInfo[] propertyInfo = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            //Act
            foreach (var property in propertyInfo)
            {
                if (property.Name.Equals("MetodoPublico"))
                {
                    result = Visivility.GetVisibility(property.GetMethod);
                }
            }

            //Assert
            result.Should().Be("Public");
        }

        [Fact]
        public void Should_Be_Private()
        {
            //Arrange
            string result = "";
            Type type = typeof(Prueba);
            PropertyInfo[] propertyInfo = type.GetProperties(BindingFlags.NonPublic | BindingFlags.Instance);

            //Act
            foreach (var property in propertyInfo)
            {
                if (property.Name.Equals("MetodoPrivado"))
                {
                    result = Visivility.GetVisibility(property.GetMethod);
                }
            }

            //Assert
            result.Should().Be("Private");
        }

        [Fact]
        public void Should_Be_Protegido()
        {
            //Arrange
            string result = "";
            Type type = typeof(Prueba);
            PropertyInfo[] propertyInfo = type.GetProperties(BindingFlags.NonPublic | BindingFlags.Instance);

            //Act
            foreach (var property in propertyInfo)
            {
                if (property.Name.Equals("MeotodoProtegido"))
                {
                    result = Visivility.GetVisibility(property.GetMethod);
                }
            }

            //Assert
            result.Should().Be("Protected");
        }

        [Fact]
        public void Should_Be_Interno()
        {
            //Arrange
            string result = "";
            Type type = typeof(Prueba);
            PropertyInfo[] propertyInfo = type.GetProperties(BindingFlags.NonPublic | BindingFlags.Instance);

            //Act
            foreach (var property in propertyInfo)
            {
                if (property.Name.Equals("MeotodoInterno"))
                {
                    result = Visivility.GetVisibility(property.GetMethod);
                }
            }

            //Assert
            result.Should().Be("Internal/Friend");
        }

        [Fact]
        public void Should_Be_ProtegidoInterno()
        {
            //Arrange
            string result = "";
            Type type = typeof(Prueba);
            PropertyInfo[] propertyInfo = type.GetProperties(BindingFlags.NonPublic | BindingFlags.Instance);

            //Act
            foreach (var property in propertyInfo)
            {
                if (property.Name.Equals("MetodoProtegidoInterno"))
                {
                    result = Visivility.GetVisibility(property.GetMethod);
                }
            }

            //Assert
            result.Should().Be("Protected Internal/Friend");
        }

    }

    class Prueba
    {
        public string MetodoPublico { get; set; }
        private string MetodoPrivado { get; set; }
        protected string MeotodoProtegido { get; set; }
        internal string MeotodoInterno { get; set; }
        protected internal string MetodoProtegidoInterno { get; set; }

    }
}
