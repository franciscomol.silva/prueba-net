﻿using ParametrosJWT.Entity;
using System.Collections.Generic;
using Xunit;

namespace UnitTestEncoderJWT.Data
{
    public class TokenData : TheoryData<Account, string>
    {
        public TokenData()
        {
            Add(new Account() { Perfiles = new string[1] { "1" }, Token = "asdfghjklñ", Usuario = "correo@correo.cl" }, "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJVc3VhcmlvIjoiY29ycmVvQGNvcnJlby5jbCIsIlBlcmZpbCI6IjEiLCJUb2tlbiI6ImFzZGZnaGprbMOxIn0.KrnONE_TPvf1oPZ4ohJFsiXhP9GLCUB-CVjdp3NoI7c");
        }

    }
}
